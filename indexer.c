#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define RED   1
#define BLACK 0

typedef struct arvoreRB {
  int qtd;
  char info[50];
  int cor;
  struct arvoreRB *esq;
  struct arvoreRB *dir;
} ArvoreRB;

typedef struct mostUsedWords {
  ArvoreRB* word;
  struct mostUsedWords *next;
} MostUsedWords;


typedef struct sortedDocuments {
  double tfIdf;
  char* documentName;
  struct sortedDocuments *next;
} SortedDocuments;

int eh_no_vermelho(ArvoreRB* no){
  if(!no) return BLACK;
  return(no->cor == RED);
}

ArvoreRB* cria_nodo(char* info){
  ArvoreRB *a = (ArvoreRB*) malloc(sizeof(ArvoreRB));
  strcpy(a->info, info);
  a->esq = a->dir = NULL;
  a->cor = RED;
  a->qtd = 1;
  return a;
}

ArvoreRB* roda_esq(ArvoreRB* esq){
  ArvoreRB *dir = esq->dir;
  esq->dir = dir->esq;
  dir->esq = esq;
  dir->cor = esq->cor;
  esq->cor = RED;
  return (dir);
}

ArvoreRB* roda_dir(ArvoreRB* dir){
  ArvoreRB *esq = dir->esq;
  dir->esq = esq->dir;
  esq->dir = dir;
  esq->cor = dir->cor;
  dir->cor = RED;
  return (esq);
}

void inverte_cores(ArvoreRB *a){
  a->cor = !(a->cor);
  a->esq->cor = !(a->esq->cor);
  a->dir->cor = !(a->dir->cor);
}

ArvoreRB* inserir(ArvoreRB *a, char* info){
  if (a == NULL)
    return cria_nodo(info);   
 
  if(strcmp(info, a->info) < 0)
    a->esq = inserir(a->esq, info);
  else if(strcmp(info, a->info) > 0)
    a->dir = inserir(a->dir, info);
  else{
    a->qtd++;
    return a;
  }
    
  if(eh_no_vermelho(a->dir) && !eh_no_vermelho(a->esq)){
    a = roda_esq(a);
  }

  if(eh_no_vermelho(a->esq) && eh_no_vermelho(a->esq->esq)){  
    a = roda_dir(a);
  }

  if(eh_no_vermelho(a->esq) && eh_no_vermelho(a->dir)){
    inverte_cores(a);
  }
 
  return a;
}


void in_order(ArvoreRB *a){
  if(!a)
    return;
  in_order(a->esq);
  printf("%d ",a->info);
  in_order(a->dir);
}

void print(ArvoreRB *a, int space) {
  if (a == NULL)
    return;

  space += 5;

  print(a->dir, space);

  printf("\n");
  for (int i = 5; i < space; i++)
      printf(" ");
  if(a->cor) printf("|");
  printf("%s (%d)\n", a->info, a->qtd);

  print(a->esq, space);
}

ArvoreRB* readFileToTree(ArvoreRB* a, int* total, char* document){
  (*total) = 0;
  int wasValid = 0;
  char word[50];
  *word = '\0';
  char buff[1024];
  FILE *file = fopen(document, "r");
  while(fgets(buff, 1024, file)) {
    for (int i = 0, j; buff[i] != '\0'; ++i) {
      if(buff[i] >= 'A' && buff[i] <= 'Z'){
        buff[i]+=' ';
      }
      if(buff[i] >= 'a' && buff[i] <= 'z'){
        wasValid = 1;
        strncat(word, &(buff[i]), 1);
      } else {
        if(wasValid && strlen(word) > 2){
          (*total)++;
          a = inserir(a, word);
          a->cor = BLACK;
        }
        *word = '\0';
        wasValid = 0;
      }
    }
    if(wasValid && strlen(word) > 2){
      (*total)++;
      a = inserir(a, word);
      a->cor = BLACK;
    }
    *word = '\0';
    wasValid = 0;
  }
  return a;
}

void getNMostUsedWordsRec(MostUsedWords** list, ArvoreRB* a, int n){
  if (a == NULL)
    return;
  
  getNMostUsedWordsRec(list, a->esq, n);

  int i = 0;
  MostUsedWords *prev, *current = *list;
  while(current != NULL && a->qtd <= current->word->qtd){
    prev = current;
    current = current->next;
    i++;
  }
  if(i < n){
    MostUsedWords* element = (MostUsedWords*)malloc(sizeof(MostUsedWords));
    element->word = a;
    if(current == *list){
      element->next = *list;
      *list = element;
    } else {
      element->next = prev->next;
      prev->next = element;
    }
    while(i < n-2 && current != NULL){
      current = current->next;
      i++;
    }
    if(current != NULL) current->next = NULL;
  }
  
  getNMostUsedWordsRec(list, a->dir, n);
}

MostUsedWords** getNMostUsedWords(ArvoreRB* a, int n){
  MostUsedWords** list = (MostUsedWords**)malloc(sizeof(MostUsedWords*));
  MostUsedWords* element = (MostUsedWords*)malloc(sizeof(MostUsedWords));
  element->word = a;
  element->next = NULL;
  *list = element;
  getNMostUsedWordsRec(list, a, n);
  return list;
}

int getWordFreq(ArvoreRB* a, char* word){
  if(a == NULL)
    return 0;
  if(!strcmp(a->info, word))
    return a->qtd;

  int freq = getWordFreq(a->esq, word);
  if(freq) return freq;

  freq = getWordFreq(a->dir, word);
  if(freq) return freq;
}

void freeList(MostUsedWords* list) {
    while (list != NULL) {
        MostUsedWords* temp = list;
        list = list->next;
        free(temp);
    }
}

int main(int argc, char** argv){// 
  if(argc >= 4){
    // --freq
    if(strcmp(argv[1], "--freq") == 0){
      ArvoreRB* a = NULL;
      int total = 0;
      a = readFileToTree(a, &total, argv[3]);
      
      int n = atoi(argv[2]);
      if(n > 0){
        MostUsedWords** list = getNMostUsedWords(a, n);
        printf("Top %d palavras mais usadas no documento: \n", n);
        MostUsedWords* element = *list;
        for(int i = 1; element != NULL; i++){
          printf("%d. %s (%d)\n", i, element->word->info, element->word->qtd);
          element = element->next;
        }
      } else printf("Argumentos invalidos");
    }
    // --

    // --freq-word
    if(strcmp(argv[1], "--freq-word") == 0){
      ArvoreRB* a = NULL;
      int total = 0;
      a = readFileToTree(a, &total, argv[3]);

      if(strlen(argv[2]) > 2){
        int freq = getWordFreq(a, argv[2]);
        printf("Frequencia da palavra \"%s\": %d", argv[2], freq);
      } else printf("Argumentos invalidos");
    }
    // --

    // --search
    if(strcmp(argv[1], "--search") == 0){
      int nDoc = argc-3;
      ArvoreRB** aList = (ArvoreRB**)malloc(sizeof(ArvoreRB*) * nDoc);
      int* totalList = (int*)malloc(sizeof(int) * nDoc);
      double* tf = (double*)malloc(sizeof(double) * nDoc);
      double* tfidf = (double*)malloc(sizeof(double) * nDoc);
      int nDocumentsWithWord = 0;
      for(int i = 0; i < nDoc; i++){
        aList[i] = NULL;
        aList[i] = readFileToTree(aList[i], &(totalList[i]), argv[i+3]);
        if(aList[i] == NULL){
          printf("Argumentos invalidos");
          return 0;
        }

        double totalTf = 0;
        char words[50];
        strcpy(words, argv[2]);
        char* word = strtok(words, " ");
        int j = 0;
        while(word != NULL){
          int freq = getWordFreq(aList[i], word);
          totalTf += (freq / 100.0) / (totalList[i] / 100.0);
          word = strtok(NULL, " ");
          j++;
        }
        tf[i] = totalTf / j;
        if(tf[i] > 0) nDocumentsWithWord++;
      }
      double idf = log10((nDoc / 100.0) / (nDocumentsWithWord / 100.0)) + 0.001;
      for(int i = 0; i < nDoc; i++){
        tfidf[i] = tf[i] * idf;
      }

      SortedDocuments** sorted = (SortedDocuments**)malloc(sizeof(SortedDocuments*));
      SortedDocuments* element = (SortedDocuments*)malloc(sizeof(SortedDocuments));
      element->documentName = argv[3];
      element->tfIdf = tfidf[0];
      element->next = NULL;
      *sorted = element;
      for(int i = 1; i < nDoc; i++){
        SortedDocuments* element = (SortedDocuments*)malloc(sizeof(SortedDocuments));
        element->documentName = argv[i+3];
        element->tfIdf = tfidf[i];
        SortedDocuments *prev, *current = *sorted;
        while(current != NULL && element->tfIdf <= current->tfIdf){
          prev = current;
          current = current->next;
        }
        if(current == (*sorted)){
          element->next = *sorted;
          *sorted = element;
        } else {
          element->next = prev->next;
          prev->next = element;
        }
      }
      
      int i = 1;
      SortedDocuments* current = *sorted;
      printf("Melhores documentos para o termo \"%s\"\n", argv[2]);
      while(current != NULL){
        printf("%d. %s\n", i, current->documentName);
        current = current->next;
        i++;
      }
    }
    //

  } else printf("Argumentos invalidos");
}
