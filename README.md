# Trabalho Final Estrutura de Dados 2
## Integrantes:
```
Gustavo Iassyl Nielsen Alves      - GRR20221083
Gustavo Jahnz                     - GRR20221117
Ingrity Valeska Cardoso Rodrigues - GRR20185215

```

## Sinopse
```
indexer --freq N ARQUIVO
indexer --freq-word PALAVRA ARQUIVO
indexer --search TERMO ARQUIVO [ARQUIVO ...]

```


## Descrição do Código

- Este programa, chamado Indexer, implementa uma contagem eficiente de palavras em documentos de texto, utilizando uma estrutura de Árvore Red-Black. Ele oferece funcionalidades como exibição de palavras mais frequentes, contagem de ocorrências de palavras específicas e busca por documentos relevantes.

## Estrutura da Árvore Red-Black

- O código define a estrutura `ArvoreRB` para representar um nodo na Árvore Red-Black. 

- Cada nodo possui informações sobre a palavra, sua quantidade de ocorrências, cor (vermelho ou preto), e ponteiros para os nodos filhos esquerdo e direito.

```
typedef struct arvoreRB {
  int qtd;
  char info[50];
  int cor;
  struct arvoreRB *esq;
  struct arvoreRB *dir;
} ArvoreRB;

```

## Funções Principais

- _`cria_nodo(char* info)`_ **: Cria um novo nodo inicializando suas propriedades.**


- _`roda_esq(ArvoreRB* esq)`_ **: Realiza uma rotação à esquerda na Árvore Red-Black.**


- _`roda_dir(ArvoreRB* dir)`_ **: Realiza uma rotação à direita na Árvore Red-Black.**


- _`inverte_cores(ArvoreRB *a)`_ **:  Inverte as cores dos nodos na Árvore Red-Black.**


- _`inserir(ArvoreRB *a, char* info)`_ **:  Insere uma nova palavra na Árvore Red-Black, mantendo suas propriedades.**


- _`in_order(ArvoreRB *a)`_ **: Realiza uma travessia em ordem na Árvore Red-Black e imprime as palavras.**


- _`print(ArvoreRB * a,int spaces)`_ **: Imprime a Árvore Red-Black com indentação.**


- _`readFileToTree(ArvoreRB* a, int* total, char* document)`_ **: Lê um arquivo de texto e insere as palavras na Árvore Red-Black.**


- _`getWordFreq(ArvoreRB* a, char* word)`_ **: Obtém a frequência de uma palavra na Árvore Red-Black.**


-  _`main(int argc, char** argv)`_ **: Função principal que processa os argumentos de linha de comando e executa as operações especificadas.**


## Processo de Execução

### Requisitos Windows

- **Ter gcc instalado.**

- **Ter o clone do projeto.**

- **Ter o minGW instalado e setado nas variáveis de ambiente do usuário (PATH -> local do bin minGW).** 


### Como Executar

  1.  Abra seu cmd no local do arquivo, após clonar o repositório.
  
  2. Execute o seguinte comando para obter o .exe do programa: _`gcc indexer.c -o indexer.exe `_

  3. Para executar o programa tenha um ou mais arquivos.txt no mesmo local em que o repositório foi clonado. Então, execute as seguintes linhas de comando:

  - _`indexer --freq N ARQUIVO`_ para obter a quantidade (N) de palavras mais utilizadas no arquivo passado.

  - _`indexer --freq-word PALAVRA ARQUIVO`_ para obter a quantidade de vezes que tal PALAVRA apresenta-se no ARQUIVO. 

  - _`indexer --search TERMO [ARQUIVO ...]`_ para obter a listagem dos arquivos mais relevantes encontrados pela busca por TERMO. [ARQUIVO...] deve ser mais de um arquivo.

